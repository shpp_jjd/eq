package com.equeue.entity;

import com.equeue.entity.enumeration.UserRole;
import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;

@Entity(name = "User")
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "user_role")
    private UserRole userRole;

    @Unique
    @Column(name = "telegram_id")
    private Long telegramId;

    @Column(name = "telegram_username")
    private String telegramUsername;

    @Column(name = "zone_id")
    private ZoneId zoneId;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Provider> providers;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Session> sessions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<StageStorage> stages;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public UserRole getUserRole() {
        return this.userRole;
    }

    public Long getTelegramId() {
        return this.telegramId;
    }

    public String getTelegramUsername() {
        return telegramUsername;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public User setUserRole(UserRole userRole) {
        this.userRole = userRole;
        return this;
    }

    public User setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
        return this;
    }

    public User setTelegramUsername(String telegramUsername) {
        this.telegramUsername = telegramUsername;
        return this;
    }

    public User setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name)
                && Objects.equals(userRole, user.userRole) && Objects.equals(telegramId, user.telegramId)
                && Objects.equals(providers, user.providers) && Objects.equals(sessions, user.sessions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, userRole, telegramId, providers, sessions);
    }

    @Override
    public String toString() {
        String nameText = "{user.text.name} - '" + name + "' \n";
        String userRoleText = "{user.text.register_as} - '" + userRole + "' \n";
        String timeZone = "{user.text.time_zone} - " + zoneId + " \n";

        return  nameText + userRoleText + timeZone;
    }
}
