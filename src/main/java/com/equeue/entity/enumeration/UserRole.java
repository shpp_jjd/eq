package com.equeue.entity.enumeration;

public enum UserRole {
    GUEST(0), CLIENT(1);

    private final Integer value;

    UserRole(Integer value) {
        this.value = value;
    }

    public Integer getValue() { return value; }
}
