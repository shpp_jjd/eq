package com.equeue.entity;

import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "stages")
public class StageStorage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    //@Unique
    private User user;

    @Column(name = "command")
    private byte[] stage;

    private Date date;

    public void setStage(byte[] command){
        this.stage =command;
    }

    public byte[] getStage(){
        return stage;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user){
        this.user=user;
    }

    public User getUser(){
        return user;
    }
}
