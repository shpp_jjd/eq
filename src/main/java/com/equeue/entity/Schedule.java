package com.equeue.entity;

import com.equeue.util.TimeUtil;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

@Entity(name = "Schedule")
@Table(name = "schedules")
public class Schedule {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @Column(name = "day_of_week")
    private Integer dayOfWeek;

    @Column(name = "work_start")
    private LocalTime workStart;

    @Column(name = "work_finish")
    private LocalTime workFinish;

    @Column(name = "duration")
    private Integer duration;

    public Provider getProvider() {
        return provider;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public LocalTime getWorkStart() {
        return workStart;
    }

    public LocalTime getWorkFinish() {
        return workFinish;
    }

    public Integer getDuration() {
        return duration;
    }

    public Schedule setProvider(Provider provider) {
        this.provider = provider;
        return this;
    }

    public Schedule setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    public Schedule setWorkStart(LocalTime workStart) {
        this.workStart = workStart;
        return this;
    }

    public Schedule setWorkFinish(LocalTime workFinish) {
        this.workFinish = workFinish;
        return this;
    }

    public Schedule setDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(provider, schedule.provider) && Objects.equals(dayOfWeek, schedule.dayOfWeek) && Objects.equals(workStart, schedule.workStart) && Objects.equals(workFinish, schedule.workFinish) && Objects.equals(duration, schedule.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, dayOfWeek, workStart, workFinish, duration);
    }

    @Override
    public String toString() {
        String providerText = "{schedule.text.schedule_for} - " + provider.getName() + "\n";
        String dayOfWeekText = "{schedule.text.week_day} - " + DayOfWeek.of(dayOfWeek) + "\n";
        String workStartText = "{schedule.text.start_work} - " + TimeUtil.getStringFromTime(
                        TimeUtil.getTimeFromUtcTimeForZone(workStart, provider.getUser().getZoneId())) + "\n";
        String workFinishText = "{schedule.text.end_work} - " + TimeUtil.getStringFromTime(
                        TimeUtil.getTimeFromUtcTimeForZone(workFinish, provider.getUser().getZoneId())) + "\n";
        String durationText = "{schedule.text.duration} - " + duration + " {schedule.text.time_unit}";

        return providerText + dayOfWeekText + workStartText + workFinishText + durationText;
    }
}
