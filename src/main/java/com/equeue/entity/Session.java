package com.equeue.entity;

import com.equeue.util.TimeUtil;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "Session")
@Table(name = "sessions")
public class Session {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "session_start")
    private LocalDateTime sessionStart;

    @Column(name = "session_finish")
    private LocalDateTime sessionFinish;

    public Long getId() {
        return id;
    }

    public Provider getProvider() {
        return provider;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getSessionStart() {
        return sessionStart;
    }

    public LocalDateTime getSessionFinish() {
        return sessionFinish;
    }

    public Session setId(Long id) {
        this.id = id;
        return this;
    }

    public Session setProvider(Provider provider) {
        this.provider = provider;
        return this;
    }

    public Session setUser(User user) {
        this.user = user;
        return this;
    }

    public Session setSessionStart(LocalDateTime sessionStart) {
        this.sessionStart = sessionStart;
        return this;
    }

    public Session setSessionFinish(LocalDateTime sessionFinish) {
        this.sessionFinish = sessionFinish;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(id, session.id) && Objects.equals(provider, session.provider) && Objects.equals(user, session.user) && Objects.equals(sessionStart, session.sessionStart) && Objects.equals(sessionFinish, session.sessionFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, provider, user, sessionStart, sessionFinish);
    }

    @Override
    public String toString() {
        return "Session { " +
                "id=" + id + ", \n" +
                "provider=" + provider.getName() + "' (" + provider.getId() + ")" + ", \n" +
                "customer=" + (user == null ? "none" : (user.getName() + "' (" + user.getId() + ")")) + ", \n" +
                "sessionStart=" + TimeUtil.getStringFromDateTime(sessionStart) + ", \n" +
                "sessionFinish=" + TimeUtil.getStringFromDateTime(sessionFinish) +
                " }";
    }

}
