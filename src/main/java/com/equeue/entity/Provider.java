package com.equeue.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity(name = "Provider")
@Table(name = "providers")
public class Provider {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "city")
    private String city;

    @OneToMany(targetEntity = com.equeue.entity.BlockedUser.class,
            mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval=true)
    private List<User> blockedUsers;

    public List<User> getBlockedUsers() {
        return blockedUsers;
    }

    public void setBlockedUsers(List<User> blockedUsers) {
        this.blockedUsers = blockedUsers;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public Provider setId(Long id) {
        this.id = id;
        return this;
    }

    public Provider setUser(User user) {
        this.user = user;
        return this;
    }

    public Provider setName(String name) {
        this.name = name;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Provider setCity(String city) {
        this.city = city;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Provider provider = (Provider) o;
        return Objects.equals(id, provider.id) && Objects.equals(user, provider.user) && Objects.equals(name, provider.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, name);
    }

    @Override
    public String toString() {
        String nameText = "{provider.text.name} - '" + name + "' \n";
        String clientText = "{provider.text.owner} - " + user.getName() + "\n";

        return nameText + clientText;
    }
}
