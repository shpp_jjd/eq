package com.equeue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

@SpringBootApplication
public class EQueueTelegramBotApplication {
	public static void main(String[] args) {
		SpringApplication.run(EQueueTelegramBotApplication.class, args);
	}
}
