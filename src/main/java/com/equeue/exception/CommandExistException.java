package com.equeue.exception;

public class CommandExistException extends CommandException{

    public CommandExistException(String msg, Long id, String language) {
        super(msg, id, language);
    }
}
