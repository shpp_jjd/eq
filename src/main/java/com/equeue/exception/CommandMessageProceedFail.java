package com.equeue.exception;

public class CommandMessageProceedFail extends CommandException{

    public CommandMessageProceedFail(String msg, Long id, String language) {
        super(msg, id, language);
    }
}
