package com.equeue.exception;

public class InvalidUsernameException extends CommandException {

    public InvalidUsernameException(String msg){
        super(msg);
    }
}
