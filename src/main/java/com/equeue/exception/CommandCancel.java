package com.equeue.exception;

import org.telegram.telegrambots.meta.api.objects.Message;

public class CommandCancel extends CommandException{

    private final Message message;

    public CommandCancel(String msg, Long id, String language) {
        super(msg, id, language);
        message=null;
    }

    public CommandCancel(String msg, Message message, String language) {
        super(msg);
        setLanguage(language);
        this.message=message;
    }

    public Message getTgMessage(){
        return message;
    }
}
