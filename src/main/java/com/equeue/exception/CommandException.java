package com.equeue.exception;

public class CommandException extends Exception{

    private Long id;
    private String language;

    public CommandException(String msg){
        super(msg);
    }

    public CommandException(String msg, Long id, String language){
        super(msg);
        this.id=id;
        this.language=language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
