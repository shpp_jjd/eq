package com.equeue.telegram_bot;

public class ButtonCommands {
    private ButtonCommands() {}


    public static final String DELETE_CLIENT_YES = "/button_delete_yes";
    public static final String DELETE_CLIENT_NO = "/button_delete_no";
    public static final String SET_TIME = "/button_set_my_time";
    public static final String SET_TIMEZONE = "/button_set_my_timezone";
    public static final String SET_FREE_TIME = "/button_set_free_time";
    public static final String CANCEL = "/cancel";

    public static final String GET_PROVIDER = "/button_get_provider";
    public static final String GET_WEEKDAY = "/button_get_weekday";
    public static final String GET_OPEN_TIME = "/button_get_open_time";
    public static final String GET_CLOSE_TIME = "/button_get_close_time";

}
