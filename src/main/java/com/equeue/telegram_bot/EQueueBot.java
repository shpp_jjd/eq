package com.equeue.telegram_bot;

import com.equeue.exception.CommandCancel;
import com.equeue.exception.CommandException;
import com.equeue.exception.CommandExistException;
import com.equeue.service.CommandChain;
import com.equeue.service.Constants;
import com.equeue.service.InternationalizationService;
import com.equeue.service.details.MessageDetailsFactory;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class EQueueBot extends TelegramLongPollingBot {
    @Value("${telegram.bot.username}")
    private String username;
    @Value("${telegram.bot.token}")
    private String token;

    private MessageSender messageSender;

    @Autowired
    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    private MessageDetailsFactory messageDetailsFactory;

    @Autowired
    public void setCommandChain(CommandChain commandChain) {
        this.commandChain = commandChain;
    }

    private CommandChain commandChain;


    @Autowired
    public void setMessageDetailsFactory(MessageDetailsFactory messageDetailsFactory) {
        this.messageDetailsFactory = messageDetailsFactory;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            var ms = messageDetailsFactory.createMessage(update);
            commandChain.proceed(ms);
        } catch (CommandCancel e) {
            proceedCancel(e);
        }catch (CommandExistException e){
            messageSender.sendIfHasText(e.getMessage(), e.getId(), e.getLanguage());
        } catch (CommandException e) {
            messageSender.sendIfHasText(Constants.SERVER_ERROR,e.getId(),e.getLanguage());
        }
    }

    private void proceedCancel(CommandCancel e){
        if (e.getTgMessage()!=null) {
            messageSender.sendUpdateMessageText(e.getTgMessage(),
                    InternationalizationService.translateTxt(e.getMessage(), e.getLanguage()));
        }else {
            messageSender.sendIfHasText(e.getMessage(), e.getId(), e.getLanguage());
        }
    }

}
