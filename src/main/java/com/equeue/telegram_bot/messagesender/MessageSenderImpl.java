package com.equeue.telegram_bot.messagesender;

import com.equeue.service.InternationalizationService;
import com.equeue.telegram_bot.EQueueBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Service
public class MessageSenderImpl implements MessageSender {

    @Autowired
    EQueueBot eQueueBot;
    @Autowired
    private InternationalizationService internationalizationService;

    private static final Logger logger = LoggerFactory.getLogger(MessageSenderImpl.class);

    @Override
    public Message sendTextTo(String messageText, Long telegramId) {
        var message = new SendMessage();
        message.setChatId(String.valueOf(telegramId));
        message.setText(messageText);
        return sendMessage(message);
    }

    @Override
    public Message sendIfHasText(String messageText, Long telegramId, String languageCode) {
        if (!messageText.equals("")){
            return sendLocalized(messageText, telegramId, languageCode);
        }
        return null;
    }

    @Override
    public Message sendLocalized(String messageText, Long telegramId, String languageCode) {
        messageText=internationalizationService.translate(messageText, languageCode);
        return sendTextTo(messageText, telegramId);
    }

    @Override
    public Message sendNotNullMsg(SendMessage sendMessage) {
        if (sendMessage!=null){
            return sendMessage(sendMessage);
        }
        return null;
    }

    public static SendMessage createMessage(String messageText, Long telegramId, String languageCode){
        var message = new SendMessage();
        message.setChatId(String.valueOf(telegramId));
        messageText=InternationalizationService.translateTxt(messageText, languageCode);
        message.setText(messageText);
        return message;
    }

    @Override
    public Message sendMessage(SendMessage sendMessage) {
        try {
           return eQueueBot.execute(sendMessage);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(),e);
        }

        return null;
    }

    @Override
    public void deleteMessage(DeleteMessage deleteMessage) {
        try {
            eQueueBot.execute(deleteMessage);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Override
    public void sendUpdateMessageText(Message message, String text) {
        var ms = new EditMessageText();
        ms.setChatId(String.valueOf(message.getChatId()));
        ms.setMessageId(message.getMessageId());
        ms.setText(text);
        sendEditedMessage(ms);
    }

    @Override
    public void sendUpdateMessage(Message message, SendMessage sendMessage) {
        var ms = new EditMessageText();
        ms.setChatId(String.valueOf(message.getChatId()));
        ms.setMessageId(message.getMessageId());
        ms.setText(sendMessage.getText());
        ms.setReplyMarkup((InlineKeyboardMarkup) sendMessage.getReplyMarkup());
        sendEditedMessage(ms);
    }

    private void sendEditedMessage(EditMessageText message){
        try{
            eQueueBot.execute(message);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(),e);
        }
    }

    public void deleteReplyMessage(Message message){
        if (message.hasReplyMarkup()){
            deleteMessage(getDeleteMessage(message.getChatId(),message.getMessageId()));
        }
    }

    public static DeleteMessage getDeleteMessage(long chatId, int messageId) {
        DeleteMessage deleteMessage = new DeleteMessage();
        deleteMessage.setChatId(String.valueOf(chatId));
        deleteMessage.setMessageId(messageId);
        return deleteMessage;
    }

}
