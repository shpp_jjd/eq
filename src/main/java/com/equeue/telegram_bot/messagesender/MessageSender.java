package com.equeue.telegram_bot.messagesender;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface MessageSender {
    Message sendTextTo(String messageText, Long telegramId);

    Message sendIfHasText(String messageText, Long telegramId, String languageCode);
    Message sendLocalized(String messageText, Long telegramId, String languageCode);

    Message sendNotNullMsg(SendMessage sendMessage);

    Message sendMessage(SendMessage sendMessage);

    void deleteMessage(DeleteMessage deleteMessage);
    void deleteReplyMessage(Message message);

    void sendUpdateMessageText(Message message, String text);
    void sendUpdateMessage(Message message, SendMessage sendMessage);


}
