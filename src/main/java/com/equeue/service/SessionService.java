package com.equeue.service;

import com.equeue.entity.Provider;
import com.equeue.entity.Schedule;
import com.equeue.entity.Session;
import com.equeue.entity.User;
import com.equeue.exception.ProviderNotFoundException;
import com.equeue.exception.UserNotFoundException;
import com.equeue.repository.ProviderRepository;
import com.equeue.repository.ScheduleRepository;
import com.equeue.repository.SessionRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.ButtonCommands;
import com.equeue.telegram_bot.Commands;
//import com.equeue.telegram_bot.services.SendMessageService;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import com.equeue.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class SessionService {

    private static final Logger logger = LoggerFactory.getLogger(SessionService.class);

    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ScheduleRepository scheduleRepository;
//    @Autowired
//    SendMessageService sendMessageService;
    @Autowired
    UserBlockingService userBlockingService;
    @Autowired
    InternationalizationService internationalizationService;

    @Transactional
    public String selectSession(MessageDetails messageDetails, String time, String providerId, String date) {
        User userById = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
        Provider providerById = providerRepository.findById(Long.parseLong(providerId)).orElseThrow(IllegalArgumentException::new);
        if (providerById == null) {
            return "{session.service.error.provider_id}: " + providerId + "{session.service.error.not_exist}";
        }
        if (scheduleRepository.findAllByProvider(providerById).isEmpty()) {
            return "{session.service.error.no_service_provider}";
        }

        LocalDate dateFromString = TimeUtil.getDateFromString(date);
        int dayOfWeek = dateFromString.getDayOfWeek().getValue();
        if (scheduleRepository.findAllByProviderAndDayOfWeek(providerById, dayOfWeek).isEmpty()) {
            return "{session.service.error.no_schedule}";
        }

        LocalDateTime userDateTime = LocalDateTime.of(TimeUtil.getDateFromString(date), TimeUtil.getTimeFromString(time));
        LocalDateTime utcDateTime = TimeUtil.getUtcDateTimeFromDateTimeAndZone(userDateTime, userById.getZoneId());

        List<Session> sessionsByProviderAndDate = sessionRepository.findAllByProviderAndSessionStart(providerById, utcDateTime);
        for (Session session : sessionsByProviderAndDate) {
            if (session.getSessionStart().toLocalTime().equals(utcDateTime.toLocalTime())) {
                if (session.getUser() != null) {
                    return "{session.service.error.busy_session}";
                }
                User customer = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
                session.setUser(customer);
                List<Session> sessions = customer.getSessions();
                sessions.add(session);
                String messageForProvider = "New record: @" + session.getUser().getTelegramUsername() +
                        "(t.me/" + session.getUser().getTelegramUsername() + ")" +
                        "\nProvider: " + session.getProvider().getName() +
                        "\nSession: " + TimeUtil.getStringFromDateTime(
                        TimeUtil.getDateTimeFromUtcDateTimeForZone(
                                session.getSessionStart(),
                               userById.getZoneId()));
                MessageSenderImpl.createMessage(messageForProvider, messageDetails.getId(), messageDetails.getLanguage());
                return "{session.service.added_session} " + date + " " + time;
            }
        }
        return "{session.service.error.no_session}";
    }

//    private String setSessionToUser(String time, String date, User user, Session session) {
//        session.setUser(user);
//        List<Session> sessions = user.getSessions();
//        sessions.add(session);
//
//        String messageForProvider = succedSevedSessionMessage(user, session);
//        Long telegramId = session.getProvider().getUser().getTelegramId();
//        sendMessageService.sendTextTo(messageForProvider, telegramId);
//        return "{session.service.added_session} " + date + " " + time;
//    }


//    public String removeSession(Message message, String time, String providerId, String date) {
//        User user = getUserFromDb(message);
//        Provider providerById = getProviderFromDb(providerId);
//
//        String errorMessage = checkIsProviderValid(providerId, date, providerById);
//        if (errorMessage != null) {
//            return errorMessage;
//        }
//        LocalDateTime userDateTime = LocalDateTime.of(TimeUtil.getDateFromString(date), TimeUtil.getTimeFromString(time));
//        LocalDateTime utcDateTime = TimeUtil.getUtcDateTimeFromDateTimeAndZone(userDateTime, user.getZoneId());
//
//        List<String> sessionsByProviderAndDate = sessionRepository.findAllByProviderAndSessionStartAndUserNotNull(providerById, utcDateTime)
//                .stream()
//                .filter(s -> s.getSessionStart().toLocalTime().equals(utcDateTime.toLocalTime()))
//                .map(s -> removeSessionFromUser(time, date, s))
//                .collect(Collectors.toList());
//
//        if (sessionsByProviderAndDate.isEmpty()) {
//            return "{session.service.error.no_session}";
//        }
//
//        return "{session.service.removed_session} " + date + " " + time;
//    }


//    @Transactional
//    public SendMessage saveSession(Message message) {
//
//        User userById = userRepository.findByTelegramId(message.getChatId()).orElseThrow(IllegalArgumentException::new);
//
//        Map<String, String> request = HelperService.parseRequest(message.getText());
//
//        if (request.size() == 1) {
//            return sendMessageService.getSendMessage(message, "{data.example}:\n" +
//                    Commands.SET_FREE_TIME + "\n" +
//                    "userName: Donald Trump\n" +
//                    "provider: BarberShop\n" +
//                    "date: 12.07.2021");
//
//        }
//
//        if (userBlockingService.isBlocked(message)) {
//            logger.info("A blocked user tries to save the session");
//            SendMessage sendMessage = new SendMessage();
//            sendMessage.setText("{session.not_exist_provider.error}");
//            sendMessage.setChatId(String.valueOf(message.getChatId()));
//            return sendMessage;
//        }
//
//        if(!request.containsKey("userName")){
//            SendMessage result = new SendMessage();
//            result.setText("{schedule.enter_provider} userName!");
//            result.setChatId(String.valueOf(message.getChatId()));
//            return result;
//        }
//        User user = userRepository.findByName(request.get("userName")).orElseThrow(IllegalArgumentException::new);
//        Provider myProvider = new Provider();
//
//        if(user.getProviders().isEmpty()){
//            SendMessage result = new SendMessage();
//            result.setText("{provider.no_services.error}");
//            result.setChatId(String.valueOf(message.getChatId()));
//            return result;
//        }
//
//        for (Provider provider: user.getProviders()) {
//            if(provider.getName().equals(request.get("provider"))){
//                myProvider = provider;
//            }
//        }
//
//        if(myProvider.getId() == null){
//            SendMessage result = new SendMessage();
//            result.setText("{provider.not_found_service.error}");
//            result.setChatId(String.valueOf(message.getChatId()));
//            return result;
//        }
//
//        final long providerId = myProvider.getId();
//        String date = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
//        if(request.containsKey("date")){
//            date = request.get("date");
//        }
//        LocalDate dateFromString = TimeUtil.getDateFromString(date);
//
//        Provider providerById = providerRepository.findById(providerId).orElseThrow(IllegalArgumentException::new);
//        if (providerById == null) {
//            return sendMessageService.getSendMessage(message, "{session.service.error.provider_id} " + providerId + "!");
//        }
//        if (scheduleRepository.findAllByProvider(providerById).isEmpty()) {
//            return sendMessageService.getSendMessage(message, "{session.service.error.no_service_provider}");
//        }
//
//        List<Session> sessionByProvider = sessionRepository.findAllByProvider(providerById).stream().collect(Collectors.toList());
//        if (sessionByProvider.isEmpty()) {
//
//            List<Session> sessions = getSessions(myProvider, dateFromString);
//            if (sessions.isEmpty()) {
//                return sendMessageService.getSendMessage(message, "{session.service.error.no_schedule}: " + date + "!");
//            }
//            sessionRepository.saveAll(sessions);
//        }
//
//        LocalDateTime dateTimeStart = LocalDateTime.of(TimeUtil.getDateFromString(date), LocalTime.of(0,0,0));
//        LocalDateTime dateTimeFinish = dateTimeStart.toLocalDate().atTime(LocalTime.MAX);
//
//        List<Session> sessionByProviderAndDate = sessionRepository.findAllByProviderAndSessionStartBetween(providerById, dateTimeStart, dateTimeFinish);
//        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
//        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
//        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
//        int iconInRow = 3;
//        for (Session session : sessionByProviderAndDate) {
//            if (session.getUser() == null) {
//                String time = TimeUtil.getStringFromTime(
//                        TimeUtil.getDateTimeFromUtcDateTimeForZone(
//                                session.getSessionStart(),
//                                userRepository.findByTelegramId(message.getFrom().getId()).orElseThrow(IllegalArgumentException::new).getZoneId()).toLocalTime());
//                InlineKeyboardButton button = new InlineKeyboardButton();
//                button.setText(time);
////                button.setCallbackData(ButtonCommands.SET_FREE_TIME + HelperService.PARAM_DIVIDER + time +
////                        HelperService.PARAM_DIVIDER + providerId + HelperService.PARAM_DIVIDER + date);
//                button.setCallbackData(time + HelperService.PARAM_DIVIDER + providerId + HelperService.PARAM_DIVIDER + date);
//                keyboardButtonList.add(button);
//            }
//            if(keyboardButtonList.size() == iconInRow){
//                rowList.add(keyboardButtonList);
//                keyboardButtonList = new ArrayList<>();
//            }
//        }
//        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
//        buttonCancel.setText("{message.cancel}");
////        buttonCancel.setCallbackData(ButtonCommands.SET_FREE_TIME + HelperService.PARAM_DIVIDER + ButtonCommands.CANCEL);
//        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
//        keyboardButtonList.add(buttonCancel);
//        rowList.add(keyboardButtonList);
//        inlineKeyboardMarkup.setKeyboard(rowList);
//        SendMessage sendMessage = new SendMessage();
//        sendMessage.setChatId(String.valueOf(message.getChatId()));
//        sendMessage.setText("{message.free_time}");
//        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
//
//        return sendMessage;
//    }

    private List<Session> getSessions(Provider myProvider, LocalDate date) {
        int dayOfWeek = date.getDayOfWeek().getValue();

        List<Schedule> scheduleList = scheduleRepository.findAllByProviderAndDayOfWeek(myProvider, dayOfWeek);
        if (scheduleList.isEmpty()) {
            return new ArrayList<>();
        }
        Schedule schedule = scheduleList.get(0);
        Provider provider = schedule.getProvider();
        Integer duration = schedule.getDuration();
        LocalTime workFinish = schedule.getWorkFinish();
        LocalTime sessionStart = schedule.getWorkStart();
        LocalTime sessionFinish = sessionStart.plusMinutes(duration);
        List<Session> generatedSessions = new ArrayList<>();
        while (sessionFinish.compareTo(workFinish) <= 0) {
            generatedSessions.add(new Session()
                    .setProvider(provider)
                    .setSessionStart(sessionStart.atDate(date))
                    .setSessionFinish(sessionFinish.atDate(date)));
            sessionStart = sessionFinish;
            sessionFinish = sessionStart.plusMinutes(duration);
        }
        return generatedSessions;
    }

    private void addGeneratedSessions(List<Session> sessions, LocalDate date) {
        for (Session session : sessions) {
            session.setSessionStart(session.getSessionStart().with(date));
            session.setSessionFinish(session.getSessionFinish().with(date));
            sessionRepository.save(session);
        }
    }

    private String getProviderNameSaveSessions(String line) {
        String[] rowTwo = line.split(" ");
        return rowTwo[1].trim();
    }

    private long getIdFromSelectSession(String[] lines, int i, String s, int i2) {
        String[] line3 = lines[i].split(s);
        return Long.parseLong(line3[i2].trim());
    }

//    private String removeSessionFromUser(String time, String date, Session session) {
//        User user = session.getUser();
//        session.setUser(null);
//
//        String messageForProvider = removeSessionMessage(user);
//        Long telegramId = session.getProvider().getUser().getTelegramId();
//        sendMessageService.sendTextTo(messageForProvider, telegramId);
//        return "{session.service.removed_session} " + date + " " + time;
//    }

    private String checkIsProviderValid(String providerId, String date, Provider providerById) {
        if (providerById == null) {
            return "{session.service.error.provider_id}: " + providerId + "{session.service.error.not_exist}";
        }
        if (scheduleRepository.findAllByProvider(providerById).isEmpty()) {
            return "{session.service.error.no_service_provider}";
        }

        LocalDate dateFromString = TimeUtil.getDateFromString(date);
        int dayOfWeek = dateFromString.getDayOfWeek().getValue();

        if (scheduleRepository.findAllByProviderAndDayOfWeek(providerById, dayOfWeek).isEmpty()) {
            return "{session.service.error.no_schedule}";
        }
        return null;
    }

    private String succedSevedSessionMessage(User userById, Session session) {
        return "New record: @" + session.getUser().getTelegramUsername() +
                "(t.me/" + session.getUser().getTelegramUsername() + ")" +
                "\nProvider: " + session.getProvider().getName() +
                "\nSession: " + TimeUtil.getStringFromDateTime(
                TimeUtil.getDateTimeFromUtcDateTimeForZone(
                        session.getSessionStart(),
                        userById.getZoneId()));
    }

    private String removeSessionMessage(User user) {
        return "(t.me/" + user.getTelegramUsername() + ")"
                + "Your session was deleted";
    }

    private Provider getProviderFromDb(String providerId) {
        long id = Long.parseLong(providerId);
        logger.info("Getting user with id {}", id);
        return providerRepository.findById(id)
                .orElseThrow(() -> {
                    logger.error("Cannot find provider with id {}", id);
                    throw new ProviderNotFoundException(String.format("Provider with id %s doesn't exist", id));
                });
    }

    private User getUserFromDb(Message message) {
        Long telegramId = message.getChatId();
        logger.info("Getting user with id {}", telegramId);
        return userRepository.findByTelegramId(telegramId)
                .orElseThrow(() -> {
                    logger.error("Cannot find user with id {}", telegramId);
                    throw new UserNotFoundException(String.format("User with id %s doesn't exist", telegramId));
                });
    }

}
