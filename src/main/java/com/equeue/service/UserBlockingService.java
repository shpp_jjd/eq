package com.equeue.service;

import com.equeue.entity.BlockedUser;
import com.equeue.entity.Provider;
import com.equeue.entity.User;
import com.equeue.repository.BlockedUserRepository;
import com.equeue.repository.ProviderRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.block_user.BlockUserTelegramId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;
import java.util.Map;

@Service
public class UserBlockingService {

    private static final Logger logger = LoggerFactory.getLogger(UserBlockingService.class);

    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    BlockedUserRepository blockedUserRepository;
//    @Autowired
//    SendMessageService sendMessageService;

    @Transactional
    public String blockUser(User userToBlock, Provider blockingProvider) {
//        Map<String, String> request = HelperService.parseRequest(message.getText());
//
//        if (request.size() == 1) {
//            return "Enter the data as:\n\n" +
//                    "/block_client\n" +
//                    "provider: BarberShop\n" +
//                    "userTelegramId: 1711371217";
//        }
//
//        User blockingUser = userRepository.findByTelegramId(message.getChatId()).orElseThrow(IllegalArgumentException::new);
//        List<Provider> providersByBlockingUser = providerRepository.findAllByUserAndName(blockingUser, request.get("provider"));
//        if (providersByBlockingUser.isEmpty()) {
//            logger.info("The specified provider is not registered for you");
//            return "{blockuser.service.provider_not_exist}";
//        }
//        Provider blockingProvider = providersByBlockingUser.get(0);
//
//        User userToBlock;
//        String telegramIdString = request.get("userTelegramId");
//        try {
//            userToBlock = getUser(telegramIdString);
//        } catch (NumberFormatException e) {
//            logger.error("Incorrect number: {}", telegramIdString);
//            return "{blockuser.service.error.parse_telegram_id}";
//        } catch (IllegalArgumentException e) {
//            logger.info("The user with the specified id is not registered");
//            return "{blockuser.service.user_not_present}";
//        }
//
//        if (!blockedUserRepository.findAllByProviderAndUser(blockingProvider, userToBlock).isEmpty()) {
//            logger.info("You are trying to block an already blocked user");
//            return "{blockuser.service.user_already_blocked}";
//        }

        if (isBlocked(userToBlock, blockingProvider)) {
            return "{blockuser.service.user_already_blocked}";
        }

        BlockedUser blockedUser = new BlockedUser();
        blockedUser.setUser(userToBlock);
        blockedUser.setProvider(blockingProvider);
        blockedUserRepository.save(blockedUser);
        return "{blockuser.service.user_blocked_succesfully}";
    }

    public boolean isBlocked(User userToBlock, Provider blockingProvider) {
        return !blockedUserRepository.findAllByProviderAndUser(blockingProvider, userToBlock).isEmpty();
    }

//    private User getUser(MessageDetails messageDetails) {
//        return userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
//    }
//
//    private User getUser(String telegramIdString) {
//        long telegramId;
//        telegramId = Long.parseLong(telegramIdString);
//        return userRepository.findByTelegramId(telegramId).orElseThrow(IllegalArgumentException::new);
//        // todo change IllegalArgumentException to UserNotFoundException
//    }
//
//    private Provider getProvider(MessageDetails messageDetails) {
//        Map<String, String> request = HelperService.parseRequest(messageDetails.getText());
//        return providerRepository.findByName(request.get("provider")).orElseThrow(IllegalArgumentException::new);
//        // todo change IllegalArgumentException to ProviderNotFoundException
//    }
//
//    public String save(User messageDetails, Long blockUserTelegramId) {
//        return ">pthf nfrjuj lkz njuj pf,kjrjdfyj";
//    }
}
