package com.equeue.service.details;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.entity.enumeration.UserRole;
import com.equeue.exception.CommandCancel;
import com.equeue.exception.CommandExistException;
import com.equeue.exception.CommandMessageProceedFail;
import com.equeue.repository.CommandRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.Constants;
import com.equeue.service.stage.Stage;
import com.equeue.telegram_bot.ButtonCommands;
import com.equeue.telegram_bot.Commands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@Service
public class MessageDetailsFactory {

    private UserRepository userRepository;
    private CommandRepository commandRepository;
    private static final Logger logger = LoggerFactory.getLogger(MessageDetailsFactory.class);

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setSessionRepository(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Transactional
    public MessageDetails createMessage(Update update) throws CommandCancel, CommandExistException, CommandMessageProceedFail {
        MessageDetailsImpl messageDetails = new MessageDetailsImpl();
        String username;

        if (update.hasCallbackQuery()) {
            username = setCallbackData(messageDetails, update.getCallbackQuery());
        } else {
            username = setMessageData(messageDetails, update.getMessage());
        }

        setUserData(messageDetails, messageDetails.getId(), username);
        checkCorrect(messageDetails);
        checkCancel(messageDetails);
        return messageDetails;
    }

    private void checkCorrect(MessageDetails messageDetails) throws CommandExistException {
        if (!messageDetails.getCommand().equals("") &&
                messageDetails.getStage() != null && !messageDetails.getCommand().equals(Commands.CANCEL)) {
            throw new CommandExistException("{command.exist.error}",
                    messageDetails.getId(), messageDetails.getLanguage());
        }
    }

    private void checkCancel(MessageDetails messageDetails) throws CommandCancel {
        if (messageDetails.getCommand().equals(ButtonCommands.CANCEL)) {
            if (messageDetails.getStage() != null) {
                commandRepository.deleteByUser(messageDetails.getUser());
                throw new CommandCancel("{command.canceled}", ((Stage)messageDetails.getStage()).getMessage(),
                        messageDetails.getLanguage());
            }else {
                throw new CommandCancel("{command.cancel.error}", messageDetails.getId(),messageDetails.getLanguage());
            }
        }
    }

    private String setCallbackData(MessageDetails messageDetails, CallbackQuery callbackQuery) {
        messageDetails.setMessage(callbackQuery.getMessage());
        messageDetails.setId(callbackQuery.getFrom().getId());
        messageDetails.setChatId(callbackQuery.getMessage().getChatId());
        messageDetails.setCommand(callbackQuery.getData());
        messageDetails.setLanguage(callbackQuery.getFrom().getLanguageCode());
        messageDetails.setMessageId(callbackQuery.getMessage().getMessageId());
        return callbackQuery.getFrom().getUserName();
    }

    private String setMessageData(MessageDetails messageDetails, Message message) {
        messageDetails.setMessage(message);
        messageDetails.setId(message.getFrom().getId());
        messageDetails.setChatId(message.getChatId());
        messageDetails.setCommand(message.getText());
        messageDetails.setLanguage(message.getFrom().getLanguageCode());
        return message.getFrom().getUserName();
    }

    private void setUserData(MessageDetails messageDetails, Long id, String username) throws CommandMessageProceedFail {
        try {
            User user = userRepository.findByTelegramId(id).
                    orElseGet(() -> registerGuest(id, username));
            messageDetails.setUser(user);

            StageStorage storage = commandRepository.findByUser(user);
            Object stage = null;
            if (storage != null) {
                stage = unwrap(storage.getStage());
            }

            messageDetails.setStage(stage);
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage(),e);
            throw new CommandMessageProceedFail("can't proceed data",messageDetails.getId(),messageDetails.getLanguage());
        }
    }

    private User registerGuest(Long tgId, String username) {
        User user = new User()
                .setName(username)
                .setUserRole(UserRole.GUEST)
                .setTelegramId(tgId)
                .setZoneId(Constants.DEFAULT_ZONE)
                .setTelegramUsername(username);
        userRepository.save(user);

        return user;
    }

    private Object unwrap(byte[] in) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bytes = new ByteArrayInputStream(in); ObjectInputStream ob = new ObjectInputStream(bytes)) {
            return ob.readObject();
        }
    }
}
