package com.equeue.service.details;

import com.equeue.entity.User;
import org.telegram.telegrambots.meta.api.objects.Message;


public interface MessageDetails {

    Long getId();
    Long getChatId();
    Integer getMessageId();
    String getText();
    Message getMessage();
    void setMessage(Message message);
    void setStage(Object stage);
    void setId(Long id);
    void setLanguage(String language);
    Object getStage();
    String getCommand();
    User getUser();
    void setUser(User user);
    String getLanguage();
    void setText(String text);
    void setCommand(String command);
    void setChatId(Long id);
    void setMessageId(Integer id);
    String getUserName();
}
