package com.equeue.service.details;

import com.equeue.entity.User;
import org.telegram.telegrambots.meta.api.objects.Message;

public class MessageDetailsImpl implements MessageDetails{

    private Long id;
    private String text="";
    private String command="";
    private Long chatId;
    private Integer messageId;
    private Object stage;
    private Message message;
    private User user;
    private String language;

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void setStage(Object stage) {
        this.stage = stage;
    }

    @Override
    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void setUser(User user){
        this.user=user;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String getUserName() {
        return user.getName();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getChatId() {
        return chatId;
    }

    @Override
    public void setChatId(Long id) {
        chatId=id;
    }

    @Override
    public Integer getMessageId() {
        return messageId;
    }

    @Override
    public void setMessageId(Integer id) {
        messageId=id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Message getMessage() {
        return message;
    }

    @Override
    public void setMessage(Message message){
        this.message=message;
    }

    @Override
    public Object getStage() {
        return stage;
    }

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public User getUser(){
        return user;
    }

    @Override
    public void setCommand(String msg){
        if (msg.startsWith("/")){
            command=msg;
        }else {
            text=msg;
        }
    }

}
