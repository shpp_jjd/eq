package com.equeue.service;

import com.equeue.service.command.*;
import com.equeue.service.details.MessageDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CommandChain {

    private Command command;

    private UserInfo userInfo;

    private DefaultCommand defaultCommand;
    private StartCommand startCommand;
    private CreateClientCommand createClientCommand;
    private CreateProviderCommand createProviderCommand;
    private CreateScheduleCommand createScheduleCommand;
    private BlockUserCommand blockUserCommand;
    private ChangeCityCommand changeCityCommand;
    private FindInCityCommand findInCityCommand;
    private ClientsListCommand clientsListCommand;
    private SetUserTimezone setUserTimezone;

    @Autowired
    public void setSetUserTimezone(SetUserTimezone setUserTimezone) {
        this.setUserTimezone = setUserTimezone;
    }

    @Autowired
    public void setFindInCityCommand(FindInCityCommand findInCityCommand) {
        this.findInCityCommand = findInCityCommand;
    }

    @Autowired
    public void setChangeCityCommand(ChangeCityCommand changeCityCommand) {
        this.changeCityCommand = changeCityCommand;
    }

    @Autowired
    public void setBlockUserCommand(BlockUserCommand blockUserCommand) {
        this.blockUserCommand = blockUserCommand;
    }

    @Autowired
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Autowired
    public void setDefaultCommand(DefaultCommand defaultCommand) {
        this.defaultCommand = defaultCommand;
    }

    @Autowired
    public void setStartCommand(StartCommand startCommand) {
        this.startCommand = startCommand;
    }

    @Autowired
    public void setCreateClient(CreateClientCommand createClientCommand) {
        this.createClientCommand = createClientCommand;
    }

    @Autowired
    public void setCreateProviderCommand(CreateProviderCommand createProviderCommand) {
        this.createProviderCommand = createProviderCommand;
    }

    @Autowired
    public void setCreateScheduleCommand(CreateScheduleCommand createScheduleCommand) {
        this.createScheduleCommand = createScheduleCommand;
    }

    @Autowired
    public void setClientsListCommand(ClientsListCommand clientsListCommand) {
        this.clientsListCommand = clientsListCommand;
    }

    // defaultCommand must be last!!!
    @PostConstruct
    public void init(){
        command = startCommand;
        command.setNext(createClientCommand)
                .setNext(createProviderCommand)
                .setNext(createScheduleCommand)
                .setNext(blockUserCommand)
                .setNext(changeCityCommand)
                .setNext(findInCityCommand)
                .setNext(userInfo)
                .setNext(clientsListCommand)
                .setNext(setUserTimezone)
                .setNext(defaultCommand);

    }

    public void proceed(MessageDetails messageDetails){
        command.proceed(messageDetails);
    }

    public CommandChain getChain() {
        return this;
    }
}
