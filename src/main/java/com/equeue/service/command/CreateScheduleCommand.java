package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.ProviderService;
import com.equeue.service.ScheduleService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.create_schedule.CreateSchedule;
import com.equeue.service.stage.create_schedule.CreateScheduleWait;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
public class CreateScheduleCommand extends Command {

    private Command next;
    private CreateSchedule stage;
    private final ProviderService providerService;
    private final ScheduleService scheduleService;

    private static final Logger logger = LoggerFactory.getLogger(CreateScheduleCommand.class);

    @Autowired
    public CreateScheduleCommand(MessageSender messageSender,
                                 CommandRepository commandRepository,
                                 ProviderService providerService,
                                 ScheduleService scheduleService){
        super(messageSender, commandRepository);
        this.providerService = providerService;
        this.scheduleService = scheduleService;
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        CreateSchedule mStage = Serializer.unwrap(messageDetails.getStage(),CreateSchedule.class);

        if (!messageDetails.getCommand().equals(Commands.CREATE_SCHEDULE) &&
                mStage == null) {
            next.proceed(messageDetails);
            return;
        }

        if (mStage != null) {
            stage = mStage;
            stage.setCommand(this);
        } else {
            stage = new CreateScheduleWait( this);
        }

        try {
            var ms = messageSender.sendNotNullMsg(stage.proceed(messageDetails));
            stage.setNotNullMessage(ms);
            stage.register(messageDetails.getUser(),commandRepository);
        }catch (CommandException | IOException e){
            logger.debug(e.getMessage());
            messageSender.sendIfHasText(e.getMessage(),messageDetails.getId(), messageDetails.getLanguage());
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (CreateSchedule) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public ProviderService getProviderService() {
        return providerService;
    }

    public ScheduleService getScheduleService() {
        return scheduleService;
    }
}
