package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.ProviderService;
import com.equeue.service.UserService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.show_clients.ClientsList;
import com.equeue.service.stage.show_clients.ClientsListWait;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
public class ClientsListCommand extends Command{


    private Command next;
    private ClientsList stage;
    private final UserService userService;
    private final ProviderService providerService;


    private static final Logger logger = LoggerFactory.getLogger(ClientsListCommand.class);

    @Autowired
    public ClientsListCommand(MessageSender messageSender,
                            CommandRepository commandRepository,
                            UserService userService,
                            ProviderService providerService){
        super(messageSender, commandRepository);
        this.userService = userService;
        this.providerService = providerService;
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        ClientsList mStage = Serializer.unwrap(messageDetails.getStage(), ClientsList.class);

        if (!messageDetails.getCommand().equals(Commands.GET_CLIENTS) &&
                mStage == null){
            next.proceed(messageDetails);
        }else {

            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new ClientsListWait(this);
            }

            try {
                var ms = messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.setNotNullMessage(ms);
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (CommandException | IOException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (ClientsList) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public UserService getUserService() {
        return userService;
    }

    public ProviderService getProviderService() {
        return providerService;
    }

}
