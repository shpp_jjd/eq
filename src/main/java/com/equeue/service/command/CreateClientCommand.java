package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.UserService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.client_register.CreateClient;
import com.equeue.service.stage.client_register.CreateClientWait;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
public class CreateClientCommand extends Command {

    private Command next;
    private CreateClient stage;
    private final UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(CreateClientCommand.class);

    @Autowired
    public CreateClientCommand(MessageSender messageSender,
                               CommandRepository commandRepository,
                               UserService userService){
        super(messageSender, commandRepository);
        this.userService = userService;
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        CreateClient mStage = Serializer.unwrap(messageDetails.getStage(), CreateClient.class);

        if (! messageDetails.getCommand().equals(Commands.CREATE_CLIENT) &&
                mStage == null){
            next.proceed(messageDetails);
        }else {

            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new CreateClientWait(this);
            }

            try {
                var ms= messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.setNotNullMessage(ms);
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (CommandException | IOException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (CreateClient)stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public UserService getUserService() {
        return userService;
    }
}
