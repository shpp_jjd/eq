package com.equeue.service.command;

import com.equeue.repository.CommandRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Stage;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserInfo extends Command{

    private Command next;

    @Autowired
    protected UserInfo(MessageSender messageSender, CommandRepository commandRepository) {
        super(messageSender, commandRepository);
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        if (!messageDetails.getCommand().equals(Commands.SHOW_CURRENT_USER_INFO)) {
            next.proceed(messageDetails);
        }else {

            messageSender.sendLocalized(messageDetails.getUser().getName(), messageDetails.getId(), messageDetails.getLanguage());
        }
    }

    @Override
    public void changeStage(Stage stage) {
        //command without stages
    }

    @Override
    public Command setNext(Command command) {
        next=command;
        return command;
    }
}
