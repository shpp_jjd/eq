package com.equeue.service.command;

import com.equeue.repository.CommandRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Stage;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultCommand extends Command {

    @Autowired
    protected DefaultCommand(MessageSender messageSender, CommandRepository commandRepository) {
        super(messageSender, commandRepository);
    }

    @Override
    public void proceed(MessageDetails messageDetails) {
        //Maybe it makes sense to clear the command stack?
        commandRepository.deleteByUser(messageDetails.getUser());
    }

    @Override
    public void changeStage(Stage stage) {
    }

    @Override
    public Command setNext(Command command) {
        return command;
    }
}
