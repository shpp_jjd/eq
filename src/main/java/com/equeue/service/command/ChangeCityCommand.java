package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.ProviderService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.block_user.BlockUser;
import com.equeue.service.stage.change_city.ChangeCity;
import com.equeue.service.stage.change_city.ChangeCityComplete;
import com.equeue.service.stage.change_city.ChangeCityWait;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
public class ChangeCityCommand extends Command {
    private Logger logger = LoggerFactory.getLogger(ChangeCityCommand.class);

    private Command next;
    private ChangeCity stage;
    private final ProviderService providerService;

    @Autowired
    protected ChangeCityCommand(MessageSender messageSender,
                                CommandRepository commandRepository,
                                ProviderService providerService) {
        super(messageSender, commandRepository);
        this.providerService = providerService;
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        ChangeCity mStage = Serializer.unwrap(messageDetails.getStage(), ChangeCity.class);

        if (! messageDetails.getCommand().equals(Commands.CHANGE_CITY) &&
                mStage == null){
            next.proceed(messageDetails);
        }else {

            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new ChangeCityWait(this);
            }

            try {
                var ms = messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.setNotNullMessage(ms);
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (CommandException | IOException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (ChangeCity) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public ProviderService getProviderService() {
        return providerService;
    }
}
