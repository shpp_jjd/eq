package com.equeue.service.command;

import com.equeue.repository.CommandRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Stage;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public abstract class Command {

    protected final MessageSender messageSender;
    protected final CommandRepository commandRepository;

    public MessageSender getMessageSender() {
        return messageSender;
    }

    @Autowired
    protected Command(MessageSender messageSender, CommandRepository commandRepository) {
        this.messageSender = messageSender;
        this.commandRepository = commandRepository;
    }

    @Transactional
    public abstract void proceed(MessageDetails messageDetails);
    public abstract void changeStage(Stage stage);
    public abstract Command setNext(Command command);


}
