package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.set_timezone.AskTime;
import com.equeue.service.stage.set_timezone.SetTimezone;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SetUserTimezone extends Command {

    private Command next;
    private SetTimezone stage;
    private final UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(SetUserTimezone.class);

    @Autowired
    protected SetUserTimezone(MessageSender messageSender, CommandRepository commandRepository, UserRepository userRepository) {
        super(messageSender, commandRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void proceed(MessageDetails messageDetails) {
        SetTimezone mStage = Serializer.unwrap(messageDetails.getStage(), SetTimezone.class);

        if (!messageDetails.getCommand().equals(Commands.SET_CURRENT_USER_TIMEZONE) &&
                mStage == null) {
            next.proceed(messageDetails);
        } else {
            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new AskTime(this);
            }

            try {
                stage.setUserRepository(userRepository);
                var ms = messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.setNotNullMessage(ms);
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (CommandException | IOException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (SetTimezone) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return command;
    }
}
