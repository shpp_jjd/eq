package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.ProviderService;
import com.equeue.service.UserBlockingService;
import com.equeue.service.UserService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.block_user.BlockUser;
import com.equeue.service.stage.block_user.BlockUserWait;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
public class BlockUserCommand extends Command {

    private Command next;
    private BlockUser stage;
    private final UserService userService;
    private final ProviderService providerService;
    private final UserBlockingService userBlockingService;

    private static final Logger logger = LoggerFactory.getLogger(BlockUserCommand.class);

    @Autowired
    public BlockUserCommand(MessageSender messageSender,
                            CommandRepository commandRepository,
                            UserService userService,
                            ProviderService providerService,
                            UserBlockingService userBlockingService){
        super(messageSender, commandRepository);
        this.userService = userService;
        this.providerService = providerService;
        this.userBlockingService = userBlockingService;
    }

    @Override
    @Transactional
    public void proceed(MessageDetails messageDetails) {
        BlockUser mStage = Serializer.unwrap(messageDetails.getStage(), BlockUser.class);

        if (! messageDetails.getCommand().equals(Commands.BLOCK_CLIENT) &&
                mStage == null){
            next.proceed(messageDetails);
        }else {

            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new BlockUserWait(this);
            }

            try {
                var ms = messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.setNotNullMessage(ms);
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (CommandException | IOException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (BlockUser) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public UserService getUserService() {
        return userService;
    }

    public ProviderService getProviderService() {
        return providerService;
    }

    public UserBlockingService getUserBlockingService() {
        return userBlockingService;
    }
}
