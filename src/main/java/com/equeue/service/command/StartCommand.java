package com.equeue.service.command;

import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.StartService;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.Stage;
import com.equeue.service.stage.start.Start;
import com.equeue.service.stage.start.StartPrintCommands;
import com.equeue.telegram_bot.Commands;
import com.equeue.telegram_bot.messagesender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class StartCommand extends Command {

    private Command next;
    private Start stage;

    private static final Logger logger = LoggerFactory.getLogger(StartCommand.class);

    private final StartService startService;

    @Autowired
    public StartCommand(MessageSender messageSender,
                        CommandRepository commandRepository,
                        StartService startService) {
        super(messageSender, commandRepository);
        this.startService = startService;
    }

    @Override
    public void proceed(MessageDetails messageDetails) {
        Start mStage = Serializer.unwrap(messageDetails.getStage(), Start.class);

        if (!messageDetails.getCommand().equals(Commands.START) &&
                mStage == null) {
            next.proceed(messageDetails);
        } else {

            if (mStage != null) {
                stage = mStage;
                stage.setCommand(this);
            } else {
                stage = new StartPrintCommands(this);
            }

            try {
                messageSender.sendNotNullMsg(stage.proceed(messageDetails));
                stage.register(messageDetails.getUser(), commandRepository);
            } catch (IOException | CommandException e) {
                logger.debug(e.getMessage());
                messageSender.sendIfHasText(e.getMessage(), messageDetails.getId(), messageDetails.getLanguage());
            }
        }
    }

    @Override
    public void changeStage(Stage stage) {
        this.stage = (Start) stage;
    }

    @Override
    public Command setNext(Command command) {
        next = command;
        return next;
    }

    public StartService getStartService() {
        return startService;
    }
}
