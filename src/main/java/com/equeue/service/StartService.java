package com.equeue.service;

import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.Commands;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class StartService {

    public SendMessage selectCommand(MessageDetails messageDetails) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        int iconInRow = 2;
        for (Map.Entry<String,String> commands: Commands.getCommandMap().entrySet()) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(InternationalizationService.translateTxt(commands.getKey(),messageDetails.getLanguage()));
            button.setCallbackData(commands.getValue());
            keyboardButtonList.add(button);

            if(keyboardButtonList.size() == iconInRow){
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }

        if(!keyboardButtonList.isEmpty()){
            rowList.add(keyboardButtonList);
        }

        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getId()));
        sendMessage.setText(InternationalizationService.translateTxt("{message.start}",messageDetails.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return sendMessage;
    }
}
