package com.equeue.service;

import java.time.ZoneId;

public interface Constants {

//    private Constants(){
//        throw new IllegalStateException("Constant class");
//    }

    public static final ZoneId DEFAULT_ZONE = ZoneId.of("Europe/Kiev");
    public static final String REGISTER_USER="{user.username}";
    public static final String REGISTER_USER_COMPLETE="{user.service.registration_congrats}";
    public static final String SERVER_ERROR="error";
    public static final String USERNAME_INVALID_ERROR ="{user.invalid_name.error}";
    public static final String USER_REGISTERED_ERROR ="{user.user_already_registered.error}";
    public static final String CANCEL="{message.cancel}";
    public static final String MSG_TIME="{message.time}";
    public static final String TIMEZONE="{message.time_zone}";
    public static final String USER_TIMEZONE="{message.your_time_zone}";

}
