package com.equeue.service;

import com.equeue.config.InternationalizationConfig;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class InternationalizationService {

    private static final String REGEXP = "\\{(.*?)}";
    private final MessageSource messageSource;


    @Autowired
    public InternationalizationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String translate(String message, String locale, Object... parameters) {
        Locale loc = Optional.ofNullable(LocaleUtils.toLocale(locale)).orElse(Locale.US);
        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(message);
        while (matcher.find()) {
            String translated = messageSource.getMessage(matcher.group(1), parameters, matcher.group(1), loc);
            if (translated != null) {
                message = message.replace(matcher.group(0), translated);
            }
        }
        return message;
    }

    public static String translateTxt(String message, String locale) {
        Locale loc = Optional.ofNullable(LocaleUtils.toLocale(locale)).orElse(Locale.US);
        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(message);
        while (matcher.find()) {
            String translated = InternationalizationConfig.RESOURCE_BUNDLE.
                    getMessage(matcher.group(1), null, matcher.group(1), loc);
            assert translated != null;
            message = message.replace(matcher.group(0), translated);
        }
        return message;
    }

}
