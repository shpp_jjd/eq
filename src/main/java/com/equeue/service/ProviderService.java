package com.equeue.service;

import com.equeue.entity.Provider;
import com.equeue.entity.User;
import com.equeue.entity.enumeration.UserRole;
import com.equeue.repository.ProviderRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.create_povider.CreateProvider;
import com.equeue.telegram_bot.ButtonCommands;
import com.equeue.telegram_bot.Commands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProviderService {
    private static final Logger logger = LoggerFactory.getLogger(ProviderService.class);
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    UserRepository userRepository;

    public String save(MessageDetails messageDetails, CreateProvider createProvider) {
//        String messageText = messageDetails.getText();
        User user = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
        if(user.getUserRole().equals(UserRole.GUEST)){
            return "{provider.unregistered.error}";
        }
//        if (messageText.replace(Commands.CREATE_PROVIDER, "").isBlank()) {
//            String data = "{data.example}";
//            return data +
//                    Commands.CREATE_PROVIDER + "\n" +
//                    "name: BarberShop";
//        }

//        String[] lines = messageText.split("\n");
//        String name = lines[1].replace("name:", "").trim();
        String name = createProvider.getName();
        if(isName(name, user)){
            return "{provider.already_registered.error}";
        }

        String city = createProvider.getCity();

        Provider provider = new Provider()
                .setUser(user)
                .setName(name)
                .setCity(city);
        providerRepository.save(provider);
        return provider.toString();
    }


    private boolean isName(String name, User user) {
        return ! providerRepository.findAllByUserAndName(user, name).isEmpty();
    }

    @Transactional
    public String findForUserByName(MessageDetails messageDetails) {
        Map<String, String> request = HelperService.parseRequest(messageDetails.getText());
        if (request.size() == 1) {
            return "{data.example}" +
                    Commands.READ_PROVIDER + "\n" +
                    "userName: Donald Trump\n" +
                    "providerName: BarberShop";
        }
        User user = userRepository.findByName(request.get("userName")).orElseThrow(IllegalArgumentException::new);

        if(!request.containsKey("providerName")){
            if(user.getProviders().isEmpty()){
                return "{provider.no_services.error}";
            }

            if(user.getProviders().size() == 1){
                return user.getProviders().get(0).toString();
            }

            return "{provider.enter_provider}";
        }

        for (Provider provider: user.getProviders()) {
            if(provider.getName().equals(request.get("providerName"))){
                return provider.toString();
            }
        }
        return "{provider.not_found_service.error}";
    }

    public boolean isProviderListEmpty(MessageDetails messageDetails){
        return providerRepository.findAllByUser(messageDetails.getUser()).isEmpty();
    }

    public SendMessage chooseProvider(MessageDetails messageDetails) {
        List<Provider> providerList = providerRepository.findAllByUser(messageDetails.getUser());

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        int iconInRow = 3;
        for (Provider provider : providerList) {

            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(provider.getName());
//            button.setCallbackData(ButtonCommands.GET_PROVIDER + HelperService.PARAM_DIVIDER + provider.getId());
            button.setCallbackData(provider.getId().toString());
            keyboardButtonList.add(button);

            if(keyboardButtonList.size() == iconInRow){
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }

        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText("{message.cancel}");
//        buttonCancel.setCallbackData(ButtonCommands.GET_PROVIDER + HelperService.PARAM_DIVIDER + ButtonCommands.CANCEL);
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        rowList.add(keyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getId()));
        sendMessage.setText("Вкажіть провайдера");
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return sendMessage;
    }

    public Optional<Provider> getProviderById(Long providerID) {
        return providerRepository.findById(providerID);
    }

    public String changeCityByProviderId(Long providerID, String city) {
        Provider provider = providerRepository.getById(providerID);
        provider.setCity(city);
        provider = providerRepository.save(provider);
        return "{change.city.success}" + provider.getCity();
    }

    public List<Provider> getAllByCity(String cityName) {
        return providerRepository.findAllByCity(cityName);
    }
}
