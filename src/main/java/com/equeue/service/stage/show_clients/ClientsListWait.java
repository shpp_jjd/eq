package com.equeue.service.stage.show_clients;

import com.equeue.entity.User;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.ClientsListCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class ClientsListWait extends ClientsList {

    public ClientsListWait(ClientsListCommand command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) {

        if (command.getProviderService().isProviderListEmpty(messageDetails)) {

            command.changeStage(null);

            return MessageSenderImpl.createMessage("{clients.list.no_providers}",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        command.changeStage(new ClientListPrint(command));

        return command.getProviderService().chooseProvider(messageDetails);
    }

    @Override
    public void register(User user, CommandRepository commandRepository) {
        //not called
    }
}
