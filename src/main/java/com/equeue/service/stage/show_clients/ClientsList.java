package com.equeue.service.stage.show_clients;

import com.equeue.service.command.ClientsListCommand;
import com.equeue.service.stage.Stage;

public abstract class ClientsList extends Stage<ClientsListCommand> {

    protected Long providerID;

    public static void setParameters(ClientsList oldObject, ClientsList newObject) {
        newObject.providerID = oldObject.providerID;
        newObject.message= oldObject.message;
    }

    public Long getProviderID() {
        return providerID;
    }
}
