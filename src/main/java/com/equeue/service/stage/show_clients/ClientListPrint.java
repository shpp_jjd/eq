package com.equeue.service.stage.show_clients;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.ClientsListCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class ClientListPrint extends ClientsList {
    public ClientListPrint(ClientsListCommand command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws InvalidUsernameException {

        this.providerID = Long.parseLong(messageDetails.getText());

        ClientsList nextCommand = new ClientsListComplete(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        String message = command.getUserService().printUsers(messageDetails);

        return MessageSenderImpl.createMessage(message,
                messageDetails.getId(),
                messageDetails.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage = new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
