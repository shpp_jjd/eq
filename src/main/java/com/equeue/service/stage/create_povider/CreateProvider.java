package com.equeue.service.stage.create_povider;

import com.equeue.service.command.CreateProviderCommand;
import com.equeue.service.stage.Stage;

public abstract class CreateProvider extends Stage<CreateProviderCommand> {

    protected String name;
    protected String city;

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public static void setParameters(CreateProvider oldObject, CreateProvider newObject) {
        newObject.name = oldObject.name;
        newObject.message = oldObject.message;
    }


}
