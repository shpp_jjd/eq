package com.equeue.service.stage.create_povider;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateProviderCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class CreateProviderName extends CreateProvider {

    public CreateProviderName(CreateProviderCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) {

        this.name = messageDetails.getText();

        CreateProvider nextCommand = new CreateProviderCity(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        return MessageSenderImpl.createMessage("{provider.text.city}",
                messageDetails.getId(),
                messageDetails.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
