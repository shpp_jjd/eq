package com.equeue.service.stage;

import java.io.*;

public class Serializer {

    private Serializer(){
        throw new IllegalStateException("Service class");
    }

    public static<T> byte[] wrap(T stage) throws IOException {
        try (ByteArrayOutputStream bytes = new ByteArrayOutputStream(); ObjectOutputStream ob = new ObjectOutputStream(bytes)){
            ob.writeObject(stage);
            return bytes.toByteArray();
        }
    }

    public static<T> T unwrap(Object o, Class<T> clazz) {
        try {
            return clazz.cast(o);
        }catch (ClassCastException e){
            return null;
        }
    }
}
