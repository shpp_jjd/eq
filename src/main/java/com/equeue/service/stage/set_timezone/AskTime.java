package com.equeue.service.stage.set_timezone;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.InternationalizationService;
import com.equeue.util.TimeUtil;
import com.equeue.service.command.Command;
import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.ButtonCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AskTime extends SetTimezone {

    public AskTime(Command command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws CommandException {
        List<LocalTime> allAvailableTime = TimeUtil.getAllAvailableTime();
        Collections.sort(allAvailableTime);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        int iconInRow = 3;
        for (LocalTime time : allAvailableTime) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(time.format(DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
            button.setCallbackData(time.format(DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
            keyboardButtonList.add(button);
            if (keyboardButtonList.size() == iconInRow) {
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }
        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText(InternationalizationService.translateTxt(Constants.CANCEL, message.getLanguage()));
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        if (!keyboardButtonList.isEmpty()) {
            rowList.add(keyboardButtonList);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(message.getId()));
        sendMessage.setText(InternationalizationService.translateTxt(Constants.MSG_TIME, message.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        nextState(new AskZone(command));
        return sendMessage;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        //unused
    }
}
