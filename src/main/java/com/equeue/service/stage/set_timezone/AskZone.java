package com.equeue.service.stage.set_timezone;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.InternationalizationService;
import com.equeue.util.TimeUtil;
import com.equeue.service.command.Command;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.ButtonCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AskZone extends SetTimezone {

    public AskZone(Command command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime userLocalDateTime = LocalDateTime.of(now.toLocalDate(), LocalTime.parse
                (message.getText(), DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
        Set<String> zones = new TreeSet<>();
        for (String id : ZoneId.getAvailableZoneIds()) {
            ZoneId zone = ZoneId.of(id);
            ZoneOffset offset = zone.getRules().getOffset(now);
            if (Math.abs(userLocalDateTime.toInstant(ZoneOffset.UTC).
                    getEpochSecond() - Instant.now().getEpochSecond() - offset.getTotalSeconds()) < 600) {
                zones.add(zone.toString());
            }
        }

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        int iconInRow = 2;
        for (String zone : zones) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(zone);
            button.setCallbackData(zone);
            keyboardButtonList.add(button);
            if (keyboardButtonList.size() == iconInRow) {
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }
        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText(InternationalizationService.translateTxt(Constants.CANCEL, message.getLanguage()));
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        if (!keyboardButtonList.isEmpty()) {
            rowList.add(keyboardButtonList);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(message.getId()));
        sendMessage.setText(InternationalizationService.translateTxt(Constants.TIMEZONE, message.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        nextState(new SetZone(command));
        command.getMessageSender().sendUpdateMessage(getMessage(),sendMessage);
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        StageStorage stageStorage = new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
