package com.equeue.service.stage.set_timezone;

import com.equeue.repository.UserRepository;
import com.equeue.service.stage.Stage;

public abstract class SetTimezone extends Stage {

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected transient UserRepository userRepository;

    protected void nextState(SetTimezone stage) {
        stage.setUserRepository(userRepository);
        stage.setMessage(message);
        command.changeStage(stage);
    }

}
