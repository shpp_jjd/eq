package com.equeue.service.stage.set_timezone;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.command.Command;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.time.ZoneId;
import java.util.Date;

public class SetZone extends SetTimezone {

    public SetZone(Command command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws CommandException {
        message.getUser().setZoneId(ZoneId.of(message.getText()));
        nextState(new Delete(command));

        command.getMessageSender().sendUpdateMessage(getMessage(),MessageSenderImpl.createMessage(Constants.USER_TIMEZONE + " - " + message.getText(),
                message.getId(), message.getLanguage()));
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        StageStorage stageStorage = commandRepository.findByUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
