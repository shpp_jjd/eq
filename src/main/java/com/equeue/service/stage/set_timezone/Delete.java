package com.equeue.service.stage.set_timezone;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.Command;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class Delete extends SetTimezone {

    public Delete(Command command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws CommandException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }
}
