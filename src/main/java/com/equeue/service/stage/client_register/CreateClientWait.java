package com.equeue.service.stage.client_register;

import com.equeue.entity.User;
import com.equeue.entity.enumeration.UserRole;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class CreateClientWait extends CreateClient {

    public CreateClientWait(CreateClientCommand client){
        command=client;
    }

    @Override

    public SendMessage proceed(MessageDetails message) {
        if (message.getUser().getUserRole() == UserRole.CLIENT) {
            command.changeStage(new CreateClientComplete(command));
            return MessageSenderImpl.createMessage(Constants.USER_REGISTERED_ERROR, message.getId(), message.getLanguage());
        }

        command.changeStage(new CreateClientName(command));
        return MessageSenderImpl.createMessage(Constants.REGISTER_USER,message.getId(),message.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository){
        //not called
    }
}
