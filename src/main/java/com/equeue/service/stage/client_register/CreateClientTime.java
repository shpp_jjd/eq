package com.equeue.service.stage.client_register;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.*;

public class CreateClientTime extends CreateClient {

    public CreateClientTime(CreateClientCommand command){
        this.command=command;
    }


    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws InvalidUsernameException {

        CreateClient nextCommand = new CreateClientTimeZone(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        command.getMessageSender().sendUpdateMessage(message,
                command.getUserService().askCurrentUserTimezone(messageDetails, messageDetails.getText()));

        return null;

    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }

}
