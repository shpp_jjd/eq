package com.equeue.service.stage.client_register;

import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.stage.Stage;

public abstract class CreateClient extends Stage<CreateClientCommand> {

    protected String name;
    protected String zone;

    public String getName() {
        return name;
    }

    public String getZone() {
        return zone;
    }

    public static void setParameters(CreateClient oldObject, CreateClient newObject){
        newObject.name = oldObject.name;
        newObject.zone = oldObject.zone;
        newObject.message= oldObject.message;
    }
}
