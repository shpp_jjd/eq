package com.equeue.service.stage.client_register;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class CreateClientName extends CreateClient {

    public CreateClientName(CreateClientCommand command){
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws InvalidUsernameException {
        String name=messageDetails.getText();
        if (!checkName(name)) {
            throw new InvalidUsernameException(Constants.USERNAME_INVALID_ERROR);
        }

        this.name = name;

        CreateClient nextCommand = new CreateClientTime(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        return command.getUserService().askCurrentUserTime(messageDetails);

    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }

    private boolean checkName(String name) {
        return name.matches("^[a-zA-Z0-9- ]+$");
    }
}
