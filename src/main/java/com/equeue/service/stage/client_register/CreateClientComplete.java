package com.equeue.service.stage.client_register;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class CreateClientComplete extends CreateClient {

    public CreateClientComplete(CreateClientCommand command){
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws CommandException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) {
        commandRepository.deleteByUser(user);
    }
}
