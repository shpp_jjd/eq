package com.equeue.service.stage;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.Command;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.io.*;

public abstract class Stage <T extends Command> implements Serializable {

    protected transient T command;

    protected Message message;

    public void setMessage(Message message) {
        this.message = message;
    }

    public void setNotNullMessage(Message message){
        if (message!=null){
            this.message = message;
        }
    }

    public Message getMessage() {
        return message;
    }

    public abstract SendMessage proceed(MessageDetails messageDetails) throws CommandException;

    /**
     * Register command session
     *
     * @param user user from MessageDetails
     * @param commandRepository repository for save current stage
     * @throws IOException exception from wrap command
     */
    public abstract void register(User user, CommandRepository commandRepository) throws IOException;

    public T getCommand() {
        return command;
    }

    public void setCommand(T command) {
        this.command = command;
    }
}
