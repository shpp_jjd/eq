package com.equeue.service.stage.block_user;

import com.equeue.entity.Provider;
import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.BlockUserCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class BlockUserTelegramId extends BlockUser {
    private Logger logger = LoggerFactory.getLogger(BlockUserTelegramId.class);

    public BlockUserTelegramId(BlockUserCommand command){
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws InvalidUsernameException {
        try {
            this.userTelegramId = Long.parseLong(messageDetails.getText());
        } catch (NumberFormatException e) {
            logger.warn("Incorrect number: {}", messageDetails.getText());
            return MessageSenderImpl.createMessage("{blockuser.service.error.parse_telegram_id}",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        User userToBlock;
        try {
            userToBlock = command.getUserService().findByTelegramId(userTelegramId);
        } catch (IllegalArgumentException e) {
            logger.info("The user with the specified id is not registered");
            return MessageSenderImpl.createMessage("{blockuser.service.user_not_present}",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        Provider blockingProvider = command.getProviderService().getProviderById(providerID).get();

        BlockUser nextCommand = new BlockUserComplete(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        return MessageSenderImpl.createMessage(command.getUserBlockingService().blockUser(userToBlock, blockingProvider),
                messageDetails.getId(),
                messageDetails.getLanguage());

    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }

}
