package com.equeue.service.stage.block_user;

import com.equeue.entity.User;
import com.equeue.entity.enumeration.UserRole;
import com.equeue.repository.CommandRepository;
import com.equeue.service.Constants;
import com.equeue.service.command.BlockUserCommand;
import com.equeue.service.command.CreateClientCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.client_register.CreateClient;
import com.equeue.service.stage.client_register.CreateClientComplete;
import com.equeue.service.stage.client_register.CreateClientName;
import com.equeue.service.stage.create_schedule.CreateScheduleProvider;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class BlockUserWait extends BlockUser {

    public BlockUserWait(BlockUserCommand command){
        this.command = command;
    }

    @Override

    public SendMessage proceed(MessageDetails messageDetails) {

        if (command.getProviderService().isProviderListEmpty(messageDetails)) {

            command.changeStage(null);

            return MessageSenderImpl.createMessage("{blockuser.service.you_are_not_provider}",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        command.changeStage(new BlockUserProvider(command));

        return command.getProviderService().chooseProvider(messageDetails);
    }

    @Override
    public void register(User user, CommandRepository commandRepository){
        //not called
    }
}
