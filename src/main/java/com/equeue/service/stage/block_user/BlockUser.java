package com.equeue.service.stage.block_user;

import com.equeue.service.command.BlockUserCommand;
import com.equeue.service.stage.Stage;

public abstract class BlockUser extends Stage<BlockUserCommand> {

    protected Long providerID;
    protected Long userTelegramId;

    public Long getProviderID() {
        return providerID;
    }

    public Long getUserTelegramId() {
        return userTelegramId;
    }

    public static void setParameters(BlockUser oldObject, BlockUser newObject) {
        newObject.providerID = oldObject.providerID;
        newObject.userTelegramId = oldObject.userTelegramId;
        newObject.message = oldObject.message;
    }
}
