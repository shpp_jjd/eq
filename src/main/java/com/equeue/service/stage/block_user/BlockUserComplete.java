package com.equeue.service.stage.block_user;

import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.BlockUserCommand;
import com.equeue.service.command.CreateProviderCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.create_povider.CreateProvider;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class BlockUserComplete extends BlockUser {

    public BlockUserComplete(BlockUserCommand command){
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws InvalidUsernameException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }
}
