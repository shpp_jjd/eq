package com.equeue.service.stage.start;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.StartCommand;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class StartPrintCommands extends Start{

    public StartPrintCommands(StartCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws CommandException {
//        return MessageSenderImpl.createMessage(String.join("\n", Commands.getCommandMap().values()),
//                                    message.getId(),
//                                    message.getLanguage());
        return command.getStartService().selectCommand(messageDetails);
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }
}
