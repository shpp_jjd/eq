package com.equeue.service.stage.change_city;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.ChangeCityCommand;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class ChangeCityComplete extends ChangeCity {

    public ChangeCityComplete(ChangeCityCommand command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws CommandException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }
}
