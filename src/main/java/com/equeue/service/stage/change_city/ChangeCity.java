package com.equeue.service.stage.change_city;

import com.equeue.service.command.ChangeCityCommand;
import com.equeue.service.stage.Stage;

public abstract class ChangeCity extends Stage<ChangeCityCommand> {
    protected Long providerID;
    protected Long userTelegramId;
    protected String city;

    public static void setParameters(ChangeCity oldObject, ChangeCity newObject){
        newObject.providerID = oldObject.providerID;
        newObject.userTelegramId = oldObject.userTelegramId;
        newObject.message= oldObject.message;
    }

}
