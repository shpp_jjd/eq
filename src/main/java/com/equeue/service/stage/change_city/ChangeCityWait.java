package com.equeue.service.stage.change_city;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.BlockUserCommand;
import com.equeue.service.command.ChangeCityCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.block_user.BlockUserProvider;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class ChangeCityWait extends ChangeCity {

    public ChangeCityWait(ChangeCityCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws CommandException {

        if (command.getProviderService().isProviderListEmpty(messageDetails)) {

            command.changeStage(null);

            return MessageSenderImpl.createMessage("{blockuser.service.you_are_not_provider}",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        command.changeStage(new ChangeCityProvider(command));

        return command.getProviderService().chooseProvider(messageDetails);

    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        // nothing to do
    }
}
