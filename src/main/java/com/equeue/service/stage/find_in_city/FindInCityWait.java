package com.equeue.service.stage.find_in_city;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.FindInCityCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.block_user.BlockUserProvider;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class FindInCityWait extends FindInCity{

    public FindInCityWait(FindInCityCommand command) {
        this.command = command;
    }


    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws CommandException {
        command.changeStage(new FindInCityDisplay(command));

        return MessageSenderImpl.createMessage("{find.in.city.where}",
                messageDetails.getId(),
                messageDetails.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        // nothing to do
    }
}
