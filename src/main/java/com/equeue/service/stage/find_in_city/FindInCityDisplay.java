package com.equeue.service.stage.find_in_city;

import com.equeue.entity.Provider;
import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.FindInCityCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.service.stage.block_user.BlockUser;
import com.equeue.service.stage.block_user.BlockUserComplete;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FindInCityDisplay extends FindInCity{

    public FindInCityDisplay(FindInCityCommand command) {
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws CommandException {
        this.cityName = messageDetails.getText();

        FindInCity nextCommand = new FindInCityComplete(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        List<Provider> providerList = command.getProviderService().getAllByCity(cityName);
        String result;
        if (providerList.isEmpty()) {
            result = "{find.in.city.no_providers}";
        } else {
            result = providerList.stream()
                    .map(provider -> provider.getName() + "\n")
                    .collect(Collectors.joining());
        }
        return MessageSenderImpl.createMessage(result,
                messageDetails.getId(),
                messageDetails.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
