package com.equeue.service.stage.find_in_city;

import com.equeue.service.command.FindInCityCommand;
import com.equeue.service.stage.Stage;

public abstract class FindInCity extends Stage<FindInCityCommand> {

    protected Long providerID;
    protected Long userTelegramId;
    protected String cityName;

    public static void setParameters(FindInCity oldObject, FindInCity newObject){
        newObject.providerID = oldObject.providerID;
        newObject.userTelegramId = oldObject.userTelegramId;
        newObject.message = oldObject.message;
    }

}
