package com.equeue.service.stage.find_in_city;

import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.FindInCityCommand;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class FindInCityComplete extends FindInCity{

    public FindInCityComplete(FindInCityCommand command){
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws InvalidUsernameException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }

}
