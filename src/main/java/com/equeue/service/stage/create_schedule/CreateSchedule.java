package com.equeue.service.stage.create_schedule;

import com.equeue.service.command.CreateScheduleCommand;
import com.equeue.service.stage.Stage;

public abstract class CreateSchedule extends Stage<CreateScheduleCommand> {

    protected Long providerId;
    protected Integer weekDay;
    protected String work_start;//LocalTime
    protected String work_finish;
    protected Integer duration;

    public static void setParameters(CreateSchedule oldObject, CreateSchedule newObject) {
        newObject.providerId = oldObject.providerId;
        newObject.weekDay = oldObject.weekDay;
        newObject.work_start = oldObject.work_start;
        newObject.work_finish = oldObject.work_finish;
        newObject.duration = oldObject.duration;
        newObject.message = oldObject.message;
    }

    public Long getProviderId() {
        return providerId;
    }

    public Integer getWeekDay() {
        return weekDay;
    }

    public String getWork_start() {
        return work_start;
    }

    public String getWork_finish() {
        return work_finish;
    }

    public Integer getDuration() {
        return duration;
    }
}
