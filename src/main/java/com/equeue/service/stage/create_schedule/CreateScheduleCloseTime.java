package com.equeue.service.stage.create_schedule;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateScheduleCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class CreateScheduleCloseTime extends CreateSchedule {

    public CreateScheduleCloseTime(CreateScheduleCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws InvalidUsernameException {

        this.work_finish = message.getText();

        CreateSchedule nextCommand = new CreateScheduleDuration(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        return MessageSenderImpl.createMessage("Скільки триває сеанс",
                message.getId(),
                message.getLanguage());
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
