package com.equeue.service.stage.create_schedule;

import com.equeue.entity.*;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateScheduleCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import com.equeue.telegram_bot.messagesender.MessageSenderImpl;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;


import java.io.IOException;
import java.util.Date;

public class CreateScheduleWait extends CreateSchedule {

    public CreateScheduleWait(CreateScheduleCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) {

        if (command.getProviderService().isProviderListEmpty(messageDetails)) {

            command.changeStage(null);

            return MessageSenderImpl.createMessage("Нема жодного провайдера",
                    messageDetails.getId(),
                    messageDetails.getLanguage());
        }

        command.changeStage(new CreateScheduleProvider(command));

        return command.getProviderService().chooseProvider(messageDetails);
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
