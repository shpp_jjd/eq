package com.equeue.service.stage.create_schedule;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import com.equeue.exception.InvalidUsernameException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateScheduleCommand;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.Serializer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;
import java.util.Date;

public class CreateScheduleProvider extends CreateSchedule {

    public CreateScheduleProvider(CreateScheduleCommand command){
        this.command = command;
    }

    @Override
    public SendMessage proceed(MessageDetails messageDetails) throws InvalidUsernameException {

//        Long providerId = Long.parseLong(message.getText());
//        Provider currentProvider = providerRepository.findById(providerId).orElse(null);
//        if (currentProvider == null) {
//            throw new InvalidUsernameException("Не знайшли ми того провайдера по айді " + providerId);
//        }


        this.providerId = Long.parseLong(messageDetails.getText());

        CreateSchedule nextCommand = new CreateScheduleWeekDay(command);
        setParameters(this, nextCommand);
        command.changeStage(nextCommand);

        return command.getScheduleService().selectWeekDay(messageDetails);
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);

        StageStorage stageStorage=new StageStorage();
        stageStorage.setUser(user);
        stageStorage.setStage(Serializer.wrap(this));
        stageStorage.setDate(new Date());
        commandRepository.save(stageStorage);
    }
}
