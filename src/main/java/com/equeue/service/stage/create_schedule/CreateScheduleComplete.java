package com.equeue.service.stage.create_schedule;

import com.equeue.entity.User;
import com.equeue.exception.CommandException;
import com.equeue.repository.CommandRepository;
import com.equeue.service.command.CreateScheduleCommand;
import com.equeue.service.details.MessageDetails;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.IOException;

public class CreateScheduleComplete extends CreateSchedule {


    public CreateScheduleComplete(CreateScheduleCommand command) {
        this.command=command;
    }

    @Override
    public SendMessage proceed(MessageDetails message) throws CommandException {
        return null;
    }

    @Override
    public void register(User user, CommandRepository commandRepository) throws IOException {
        commandRepository.deleteByUser(user);
    }
}
