package com.equeue.service;

import com.equeue.entity.Provider;
import com.equeue.entity.User;
import com.equeue.entity.enumeration.UserRole;
import com.equeue.exception.UserNotFoundException;
import com.equeue.repository.ProviderRepository;
import com.equeue.repository.ScheduleRepository;
import com.equeue.repository.SessionRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.client_register.CreateClient;
import com.equeue.telegram_bot.ButtonCommands;
import com.equeue.telegram_bot.Commands;
import com.equeue.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    public static final ZoneId DEFAULT_ZONE = ZoneId.of("Europe/Kiev");
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    ScheduleRepository scheduleRepository;

    public String save(MessageDetails messageDetails, CreateClient createClient) {
        String name = createClient.getName();

        if (!checkName(name)) {
            return InternationalizationService.translateTxt("{user.invalid_name.error}",
                    messageDetails.getLanguage());
        }

        User currentUser = userRepository.findByName(name).orElse(null);
        if (currentUser != null) {
            return InternationalizationService.translateTxt("{user.name_already_exists.error}",
                    messageDetails.getLanguage());
        }

        currentUser = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
        if (currentUser.getUserRole() == UserRole.CLIENT) {
            String data = InternationalizationService.translateTxt("{user.user_already_registered.error}",
                    messageDetails.getLanguage());
            String nameText = InternationalizationService.translateTxt("{user.username}",messageDetails.getLanguage());
            return data +
                    nameText + currentUser.getName();
        }

        currentUser.setName(name).setUserRole(UserRole.CLIENT);

        currentUser.setZoneId(ZoneId.of(createClient.getZone()));

        userRepository.save(currentUser);
        return InternationalizationService.translateTxt("{user.service.registration_congrats}",
                messageDetails.getLanguage()) +"\n"+
                InternationalizationService.translateTxt("{user.username} ",messageDetails.getLanguage()) + name;
    }

    public String deleteUser(MessageDetails messageDetails) {
        User user = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
        Long id = user.getId();
        if (id == null) {
            return InternationalizationService.translateTxt("{user.nor_registered}", messageDetails.getLanguage());
        }

        userRepository.deleteById(id);
        return "Вы удалили все свои данные!";
    }

    private boolean checkName(String name) {
        return name.matches("^[a-zA-Z0-9- ]+$");
    }

    public String findById(MessageDetails messageDetails) {
        String messageText = messageDetails.getText();
        if (messageText.replace(Commands.READ_CLIENT, "").isBlank()) {
            return "Введите данные в виде:\n" +
                    Commands.READ_CLIENT + "\n" +
                    "clientName: Donald Trump";
        }

        String[] lines = messageText.split("\n");
        String userName = lines[1].replace("clientName:", "").trim();
        return userRepository.findByName(userName).toString();
    }

    public void registerGuestUserIfNotExist(MessageDetails messageDetails) {
        Long tgId = messageDetails.getId();
        if (userRepository.findByTelegramId(tgId).isEmpty()) {
            String tgUsername = messageDetails.getUserName();
            userRepository.save(new User()
                    .setName(tgUsername)
                    .setUserRole(UserRole.GUEST)
                    .setTelegramId(tgId)
                    .setZoneId(DEFAULT_ZONE)
                    .setTelegramUsername(tgUsername));
        }
    }

    public SendMessage askOrDeleteUser(MessageDetails messageDetails) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton buttonYes = new InlineKeyboardButton();
        buttonYes.setText(InternationalizationService.translateTxt("{message.yes}",messageDetails.getLanguage()));
        buttonYes.setCallbackData(ButtonCommands.DELETE_CLIENT_YES);
        InlineKeyboardButton buttonNo = new InlineKeyboardButton();
        buttonNo.setText(InternationalizationService.translateTxt("{message.no}",messageDetails.getLanguage()));
        buttonNo.setCallbackData(ButtonCommands.DELETE_CLIENT_NO);

        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        keyboardButtonList.add(buttonYes);
        keyboardButtonList.add(buttonNo);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(rowList);

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getChatId()));
        sendMessage.setText(InternationalizationService.translateTxt("{user.service.delete_data_confirm}",messageDetails.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    public SendMessage askCurrentUserTime(MessageDetails messageDetails) {
        List<LocalTime> allAvailableTime = TimeUtil.getAllAvailableTime();
        Collections.sort(allAvailableTime);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        int iconInRow = 3;
        for (LocalTime time: allAvailableTime) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(time.format(DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
//            button.setCallbackData(ButtonCommands.SET_TIME + HelperService.PARAM_DIVIDER +
//                    time.format(DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
            button.setCallbackData(time.format(DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
            keyboardButtonList.add(button);
            if(keyboardButtonList.size() == iconInRow){
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }
        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText(InternationalizationService.translateTxt("{message.cancel}",messageDetails.getLanguage()));
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        if(!keyboardButtonList.isEmpty()){
            rowList.add(keyboardButtonList);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getChatId()));
        sendMessage.setText(InternationalizationService.translateTxt("{message.time}",messageDetails.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return sendMessage;
    }

    public SendMessage askCurrentUserTimezone(MessageDetails messageDetails, String timeString) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime userLocalDateTime = LocalDateTime.of(now.toLocalDate(), LocalTime.parse
                (timeString, DateTimeFormatter.ofPattern(TimeUtil.TIME_PATTERN)));
        Set<String> zones = new TreeSet<>();
        for (String id : ZoneId.getAvailableZoneIds()) {
            ZoneId zone = ZoneId.of(id);
            ZoneOffset offset = zone.getRules().getOffset(now);
            if (Math.abs(userLocalDateTime.toInstant(ZoneOffset.UTC).
                    getEpochSecond() - Instant.now().getEpochSecond() - offset.getTotalSeconds()) < 600) {
                zones.add(zone.toString());
            }
        }

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        int iconInRow = 2;
        for (String zone: zones) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(zone);
            //button.setCallbackData(ButtonCommands.SET_TIMEZONE + HelperService.PARAM_DIVIDER + zone);
            button.setCallbackData(zone);
            keyboardButtonList.add(button);
            if(keyboardButtonList.size() == iconInRow){
                rowList.add(keyboardButtonList);
                keyboardButtonList = new ArrayList<>();
            }
        }
        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText(InternationalizationService.translateTxt("{message.cancel}",messageDetails.getLanguage()));
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        if(!keyboardButtonList.isEmpty()){
            rowList.add(keyboardButtonList);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getChatId()));
        sendMessage.setText(InternationalizationService.translateTxt("{message.time_zone}",messageDetails.getLanguage()));
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return sendMessage;
    }

    public String setCurrentUserTimezone(MessageDetails messageDetails, String zone){
        try {
            User byId = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
            byId.setZoneId(ZoneId.of(zone));
        } catch (Exception e) {
            return "FAIL";
        }
        return InternationalizationService.translateTxt("{message.your_time_zone} - ",messageDetails.getLanguage()) + zone;
    }

    public SendMessage askCurrentUserLocale(MessageDetails messageDetails) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getChatId()));
        sendMessage.setText(messageDetails.getLanguage());
        return sendMessage;
    }

    public User findByTelegramId(Long chatId) {
        return userRepository.findByTelegramId(chatId).orElseThrow(IllegalArgumentException::new);
    }

    public String printUsers(MessageDetails message){
        Provider provider = providerRepository.findByUser_TelegramId(message.getChatId())
                .orElseThrow(() -> new UserNotFoundException("Cannot find user"));

        String text = userRepository.findAllProviderActiveUsers(provider.getId())
                .stream()
                .map(u -> "@" + u.getTelegramUsername())
                .collect(Collectors.joining("\n"));
        if(text.isEmpty()){
            text = "No users :(";
        }

        return text;
    }

}
