package com.equeue.service;

import com.equeue.entity.Schedule;
import com.equeue.repository.ProviderRepository;
import com.equeue.repository.ScheduleRepository;
import com.equeue.repository.UserRepository;
import com.equeue.service.details.MessageDetails;
import com.equeue.service.stage.create_schedule.CreateSchedule;
import com.equeue.telegram_bot.ButtonCommands;
import com.equeue.telegram_bot.Commands;
import com.equeue.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ScheduleService {
    private static final Logger logger = LoggerFactory.getLogger(ScheduleService.class);
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProviderRepository providerRepository;

    @Transactional
    public String save(MessageDetails messageDetails, CreateSchedule createSchedule) {
//        Map<String, String> request = HelperService.parseRequest(message.getText());
//        if (request.size() == 1) {
//            return "{data.example}" +
//                    Commands.CREATE_SCHEDULE + "\n" +
//                    "provider: BarberShop\n" +
//                    "dayOfWeek: 1\n" +
//                    "workStart: 9:00\n" +
//                    "workFinish: 18:00\n" +
//                    "duration: 30";
//        }
//        Provider provider;
//        User user = userRepository.findByTelegramId(messageDetails.getChatId()).orElseThrow(IllegalArgumentException::new);
//        if(user.getProviders().isEmpty()){
//            return "{schedule.no_provider.error}";
//        } else if (user.getProviders().size() == 1){
//            provider = user.getProviders().get(0);
//        }else if (user.getProviders().size() > 1 && !request.containsKey("provider")){
//            return "{schedule.enter_provider} (provider:)!";
//        }else{
//            provider = providerRepository.findByName(request.get("provider")).orElseThrow(IllegalArgumentException::new);
//        }

        Schedule schedule = new Schedule();
        schedule
                .setProvider(providerRepository.getById(createSchedule.getProviderId()))
                .setDayOfWeek(createSchedule.getWeekDay())
                .setWorkStart(TimeUtil.getUtcTimeFromTimeAndZone(
                        TimeUtil.getTimeFromString(createSchedule.getWork_start()),
                        userRepository.findByTelegramId(messageDetails.getId()).orElseThrow(IllegalArgumentException::new).getZoneId()))
                .setWorkFinish(TimeUtil.getUtcTimeFromTimeAndZone(
                        TimeUtil.getTimeFromString(createSchedule.getWork_finish()),
                        userRepository.findByTelegramId(messageDetails.getId()).orElseThrow(IllegalArgumentException::new).getZoneId()))
                .setDuration(createSchedule.getDuration());
        scheduleRepository.save(schedule);
        return schedule.toString();
    }


    public SendMessage selectWeekDay(MessageDetails messageDetails) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonList = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        for (int i = 1; i <= 7; i++) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText("" + i);
//            button.setCallbackData(ButtonCommands.GET_WEEKDAY + HelperService.PARAM_DIVIDER + i);
            button.setCallbackData("" + i);
            keyboardButtonList.add(button);
        }

        InlineKeyboardButton buttonCancel = new InlineKeyboardButton();
        buttonCancel.setText("{message.cancel}");
//        buttonCancel.setCallbackData(ButtonCommands.GET_WEEKDAY + HelperService.PARAM_DIVIDER + ButtonCommands.CANCEL);
        buttonCancel.setCallbackData(ButtonCommands.CANCEL);
        keyboardButtonList.add(buttonCancel);
        rowList.add(keyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(rowList);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(messageDetails.getId()));
        sendMessage.setText("Вкажіть день тижня");
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return sendMessage;
    }
}
