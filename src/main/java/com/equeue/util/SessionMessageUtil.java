package com.equeue.util;

import com.equeue.service.details.MessageDetails;
import com.equeue.telegram_bot.Commands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public class SessionMessageUtil {


    public static SendMessage messageNotContainsName(Message message) {
        SendMessage result = new SendMessage();
        result.setText("{schedule.enter_provider} userName!");
        result.setChatId(String.valueOf(message.getChatId()));
        return result;
    }

    public static SendMessage messageNoProviders(Message message){
        SendMessage result = new SendMessage();
        result.setText("{provider.no_services.error}");
        result.setChatId(String.valueOf(message.getChatId()));
        return result;
    }
}
