package com.equeue.repository;

import com.equeue.entity.StageStorage;
import com.equeue.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface CommandRepository extends CrudRepository<StageStorage,Long> {
    void deleteByUser(User user);
    StageStorage findByUser(User user);
}
