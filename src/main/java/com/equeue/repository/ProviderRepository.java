package com.equeue.repository;

import com.equeue.entity.Provider;
import com.equeue.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProviderRepository extends JpaRepository<Provider, Long> {

    List<Provider> findAllByUserAndName(User user, String name);

    Optional<Provider> findByName(String name);

    Optional<Provider> findByUser_TelegramId(Long id);

    List<Provider> findAllByUser(User user);

    List<Provider> findAllByCity(String city);
}
