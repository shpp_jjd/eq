package com.equeue.repository;

import com.equeue.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByTelegramId(Long id);

    Optional<User> findByName(String login);

    @Query(value = "select * from users join providers p on users.id = p.user_id join sessions s on users.id = s.user_id\n" +
            "where s.user_id notnull " +
            "and s.session_start >= now()", nativeQuery = true)
    List<User> findAllProviderActiveUsers(Long id);
}
