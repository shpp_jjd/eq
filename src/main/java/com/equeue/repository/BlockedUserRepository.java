package com.equeue.repository;

import com.equeue.entity.BlockedUser;
import com.equeue.entity.Provider;
import com.equeue.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlockedUserRepository extends JpaRepository<BlockedUser, Long> {

    List<BlockedUser> findAllByProviderAndUser(Provider provider, User user);
}