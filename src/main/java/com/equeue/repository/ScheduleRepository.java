package com.equeue.repository;

import com.equeue.entity.Provider;
import com.equeue.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    List<Schedule> findAllByProvider(Provider provider);

    List<Schedule> findAllByProviderAndDayOfWeek(Provider provider, Integer value);

}
