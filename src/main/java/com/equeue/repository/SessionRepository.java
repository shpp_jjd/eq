package com.equeue.repository;

import com.equeue.entity.Provider;
import com.equeue.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface SessionRepository extends JpaRepository<Session, Long> {

    List<Session> findAllByProvider(Provider provider);

    List<Session> findAllByProviderAndSessionStart(Provider provider, LocalDateTime dateTimeStart);

    List<Session> findAllByProviderAndSessionStartAndUserNotNull(Provider provider, LocalDateTime dateTimeStart);

    List<Session> findAllByProviderAndSessionStartAndUserIsNull(Provider provider, LocalDateTime dateTimeStart);

    List<Session> findAllByProviderAndSessionStartBetween(Provider provider, LocalDateTime dateTimeStart, LocalDateTime dateTimeFinish);

}
