-- auto-generated definition
create table USER
(
    ID                LONG auto_increment,
    NAME              VARCHAR(100),
    USER_ROLE         SMALLINT     not null,
    TELEGRAM_ID       LONG         not null,
    TELEGRAM_USERNAME VARCHAR(100) not null,
    ZONE_ID           VARCHAR(3),
    constraint USER_PK
        primary key (ID)
);

create unique index USER_ID_UINDEX
    on USER (ID);

create unique index USER_TELEGRAM_ID_UINDEX
    on USER (TELEGRAM_ID);

create table PROVIDER
(
    ID      LONG auto_increment,
    USER_ID LONG not null,
    NAME    VARCHAR(100),
    constraint PROVIDER_PK
        primary key (ID)
);

create unique index PROVIDER_ID_UINDEX
    on PROVIDER (ID);

create table SESSION
(
    ID             LONG auto_increment,
    PROVIDER_ID    LONG     not null,
    USER_ID        LONG     not null,
    SESSION_START  DATETIME not null,
    SESSION_FINISH DATETIME not null,
    constraint SESSION_PK
        primary key (ID)
);

create unique index SESSION_ID_UINDEX
    on SESSION (ID);

create table SCHEDULE
(
    ID          LONG auto_increment,
    PROVIDER_ID LONG not null,
    DAY_OF_WEEK INT  not null,
    WORK_START  TIME not null,
    WORK_FINISH TIME not null,
    DURATION    INT  not null,
    constraint SCHEDULE_PK
        primary key (ID)
);

create unique index SCHEDULE_ID_UINDEX
    on SCHEDULE (ID);